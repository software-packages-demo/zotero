# zotero

Collect, organize, cite, and share research https://zotero.org

# Official documentation
* https://zotero.org/support/quick_start_guide

# Unofficial documentation
## Tutorial
* [zotero tutorial](https://www.google.com/search?q=zotero+tutorial)

# Installation
## Debian based systems
* The `libreoffice-java-common` deb package may be required to run the Libreoffice plugin.

## Docker image systems
* Zotero depends on DBus, which may need to be started using its init script. OpenRC can be used for that, if available.
  * `error: Could not connect: No such file or directory` This error may be a result of DBus not working as required.
